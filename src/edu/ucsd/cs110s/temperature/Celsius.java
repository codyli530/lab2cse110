/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author lol002
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return Float.toString(this.getValue());
	}
	@Override
	public Temperature toCelsius() {
		
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(this.getValue() * 9.0f / 5.0f + 32.0f);
	}
}
