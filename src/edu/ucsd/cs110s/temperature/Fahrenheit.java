/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author lol002
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return Float.toString(this.getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius((this.getValue()-32.0f) * 5.0f / 9.0f);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
}
